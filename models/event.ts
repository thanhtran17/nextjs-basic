export type EventType = {
  id: number;
  title: string;
  description: string;
  category: string;
  date: string;
};
