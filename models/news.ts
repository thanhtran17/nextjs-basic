export type NewsType = {
  id: number;
  title: string;
  description: string;
  category: string;
};
