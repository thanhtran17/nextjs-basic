export type DashboardDataType = {
  posts: number;
  likes: number;
  followers: number;
  following: number;
};
