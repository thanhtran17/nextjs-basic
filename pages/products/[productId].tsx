import { GetStaticPaths, GetStaticProps } from "next";
import { useRouter } from "next/router";
import React from "react";
import { ProductType } from "../../models/product";

type Props = {
  product: ProductType;
};

const Product: React.FC<Props> = ({ product }) => {
  const router = useRouter();

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h2>
        {product.id} {product.title} {product.price}
      </h2>
      <p>{product.description}</p>
      <hr />
    </div>
  );
};

export default Product;

export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: [{ params: { productId: "1" } }],
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async (context) => {
  const { params } = context;

  if (params) {
    console.log(`Generating/Regenerating Product ${params.productId}`);
    const res = await fetch(
      `http://localhost:4000/products/${params.productId}`
    );

    const data = await res.json();

    return {
      props: {
        product: data,
      },
      revalidate: 10,
    };
  }

  return {
    props: {
      product: null,
    },
  };
};
