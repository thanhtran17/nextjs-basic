import { GetStaticProps } from "next";
import Link from "next/link";
import React from "react";
import { ProductType } from "../../models/product";

type Props = {
  products: ProductType[];
};

const ProductList: React.FC<Props> = ({ products }) => {
  return (
    <>
      <h1>LIST OF PRODUCTS</h1>
      {products.map((product) => {
        return (
          <div key={product.id}>
            <Link href={`/products/${product.id}`} passHref>
              <h2>
                {product.id} {product.title} {product.price}
              </h2>
            </Link>
            <hr />
          </div>
        );
      })}
    </>
  );
};

export default ProductList;

export const getStaticProps: GetStaticProps = async () => {
  console.log("Generating/Regenerating ProductList");
  const res = await fetch("http://localhost:4000/products");
  const data = (await res.json()) as ProductType[];

  return {
    props: {
      products: data,
    },
    revalidate: 10,
  };
};
