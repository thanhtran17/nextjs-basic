import React from "react";
import User from "../components/user";

const UserList = (props: any) => {
  const { users } = props;

  return (
    <>
      <h1>List:</h1>
      {users.map((user: any) => {
        return (
          <div key={user.id}>
            <User user={user} />
          </div>
        );
      })}
    </>
  );
};

export default UserList;

export async function getStaticProps() {
  const response = await fetch("https://jsonplaceholder.typicode.com/users");
  const data = await response.json();

  console.log(data);

  return {
    props: {
      users: data,
    },
  };
}
