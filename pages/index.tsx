import Link from "next/link";

function Home() {
  return (
    <>
      <h1>HomePage</h1>
      <Link href="/posts">
        <a>Posts</a>
      </Link>
    </>
  );
}

export default Home;
