import { GetServerSideProps } from "next";
import React from "react";
import { NewsType } from "../../models/news";

type Props = {
  articles: NewsType[];
};

const NewsArticleList: React.FC<Props> = ({ articles }) => {
  return (
    <>
      <h1>List of News Articles</h1>
      {articles.map((article) => {
        return (
          <div key={article.id}>
            <h2>
              {article.id} | {article.title} | {article.category}
            </h2>
          </div>
        );
      })}
    </>
  );
};

export default NewsArticleList;

export const getServerSideProps: GetServerSideProps = async () => {
  const response = await fetch("http://localhost:4000/news");
  const data = (await response.json()) as NewsType[];

  console.log("Pre-rendering NewsArticleList");

  return {
    props: {
      articles: data,
    },
  };
};
