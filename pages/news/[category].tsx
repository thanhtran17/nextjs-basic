import { GetServerSideProps } from "next";
import React from "react";
import { NewsType } from "../../models/news";

type Props = {
  articles: NewsType[];
  category: string;
};

const ArticleListByCategory: React.FC<Props> = ({ articles, category }) => {
  return (
    <>
      <h1>Article list by {category} category </h1>
      {articles.map((article) => {
        return (
          <div key={article.id}>
            <h1>
              {article.id} {article.title}
            </h1>
            <p>{article.description}</p>
            <hr />
          </div>
        );
      })}
    </>
  );
};

export default ArticleListByCategory;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { params, req, res, query } = context;

  // console.log(req.headers.cookie);
  // console.log(query);

  res.setHeader("Set-Cookie", ["name=Thanh"]);

  if (params) {
    const { category } = params;

    const response = await fetch(
      `http://localhost:4000/news?category=${category}`
    );
    const data = (await response.json()) as NewsType[];

    console.log(`Pre-rendering NewsArticleListByCate ${category}`);

    return {
      props: {
        articles: data,
        category,
      },
    };
  }

  return {
    props: {
      articles: [] as NewsType[],
      category: "",
    },
  };
};
