import React from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { PostType } from "../../models/post";
import { useRouter } from "next/router";

type Props = {
  post: PostType;
};

const Post: React.FC<Props> = ({ post }) => {
  const router = useRouter();

  if (router.isFallback) {
    return <h1>Loading...</h1>;
  }

  return (
    <>
      <h2>
        {post.id} {post.title}
      </h2>
      <p>{post.body}</p>
    </>
  );
};

export default Post;

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data = (await response.json()) as PostType[];

  const paths = data.slice(0, 3).map((post: PostType) => {
    return {
      params: {
        postId: `${post.id}`,
      },
    };
  });

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps: GetStaticProps = async (context) => {
  const { params } = context;

  if (params) {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${params.postId}`
    );

    const data = (await response.json()) as PostType;

    if (!data.id) {
      return {
        notFound: true,
      };
    }

    console.log(`Generating page for /posts/${params.postId}`);

    return {
      props: {
        post: data,
      },
    };
  }

  return {
    props: {
      posts: [] as PostType[],
    },
  };
};
