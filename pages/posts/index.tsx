import Link from "next/link";
import React from "react";
import { PostType } from "../../models/post";

type Props = {
  posts: PostType[];
};

const PostList: React.FC<Props> = ({ posts }) => {
  return (
    <>
      <h1 style={{ textTransform: "uppercase" }}>List Of Posts</h1>

      {posts.map((post: PostType) => {
        return (
          <Link href={`posts/${post.id}`} passHref key={post.id}>
            <h2>
              {post.id} {post.title}fsdf
            </h2>
          </Link>
        );
      })}
    </>
  );
};

export default PostList;

export async function getStaticProps() {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data = (await response.json()) as PostType[];

  return {
    props: {
      posts: data,
    },
  };
}
