import { GetServerSideProps } from "next";
import React, { useState } from "react";
import { EventType } from "../models/event";
import { useRouter } from "next/router";

type Props = {
  events: EventType[];
};

const Events: React.FC<Props> = ({ events }) => {
  const [eventsFiltered, setEventsFiltered] = useState(events);
  const router = useRouter();

  const fetchSportsEvents = async () => {
    const res = await fetch("http://localhost:4000/events?category=sports");
    const data = (await res.json()) as EventType[];
    setEventsFiltered(data);
    router.push("events?category=sports", undefined, { shallow: true });
  };

  return (
    <>
      <button onClick={fetchSportsEvents}>Sports Events</button>
      <h1>List of Event</h1>
      {eventsFiltered.map((event) => {
        return (
          <div key={event.id}>
            <h2>
              {event.id} | {event.title} | {event.date} | {event.category}
            </h2>
            <p>{event.description}</p>
          </div>
        );
      })}
    </>
  );
};

export default Events;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context;
  const { category } = query;

  const queryString = category ? "category=sports" : "";

  const res = await fetch(`http://localhost:4000/events?${queryString}`);
  const data = (await res.json()) as EventType[];

  return {
    props: {
      events: data,
    },
  };
};
